FROM php:8.1-apache
RUN apt-get update; \
    apt-get install -y default-mysql-client libzip-dev;
RUN docker-php-ext-install mysqli pdo pdo_mysql zip; \
    docker-php-ext-enable mysqli;
RUN a2enmod rewrite; \
    service apache2 restart;

