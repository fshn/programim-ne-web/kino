<?php
session_start();
require_once("./lib/connect.php");

// redirect if there is no session
if (!isset($_SESSION["first_name"])) {
    header("Location: login/login.php");
    die();
}

require_once("./lib/connect.php");
require_once("./lib/header.php");
?>
<link rel="stylesheet" type="text/css" href="./resource/static/css/admin.css">

<title>Dashboard</title>
</head>
<body>
<div class="container1">
    <div class="nav-bar" id="nav-bar">
        <img class="logo" src="../resource/media/img/blackGold.png">
        <a class="triger-a" href="javascript:void(0);" class="icon" onclick="myFunction()">
            <i class="fa fa-bars"></i>
        </a>
        <hr class="hr-nav">
        <ul class="ul-nav">

            <li><h6>Dashboard</h6></li>
            <li><a href="dashboard.php"><i class="fa fa-user"></i> Profile</a></li>
            <li><a href="./index.php"><i class="fa fa-film"></i> Catalogue</a></li>
            <li><a href="./user/reset.php"><i class="fa fa-sliders"></i> Settings</a></li>
            <li><a href="logout.php"><i class="fa fa-power-off"></i> Log Out</a></li>

        </ul>
    </div>
    <div>
        <div class="container2" id="container2">
            <div class="logout-nav">
                <a class="triger-b" href="javascript:void(0);" class="icon" onclick="myFunction()">
                    <i class="fa fa-bars"></i>
                </a>
                <h5> Hello <?php echo $_SESSION["first_name"] ?> <i class="fa fa-user"></i></h5>

            </div>
            <div class="div-body">
                <h2>Data:</h2>
                <?php
                echo sprintf("Welcome %s! You made it.<br>", $_SESSION["first_name"]);
                echo "This data is stored in your session:<br>";
                print_r($_SESSION);
                ?>
            </div>
        </div>
        <div class="container2">
            <div class="div-body">
                <h2>Purchases:</h2>
                <hr>
                <table style="text-align: center">
                    <thead>
                    <tr>
                        <th>Film</th>
                        <th>Date</th>
                        <th>Download</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $id = mysqli_real_escape_string($conn, $_SESSION["user_id"]);
                    $query = <<<QUERY
                        select `film`.`id` as `film_id`, `film`.`title` as `title`, `purchase`.`purchase_date` as `date`
                        from `user` inner join `purchase`
                                        on `user`.`id` = `purchase`.`user_id`
                                    inner join `film`
                                        on `film`.`id` = `purchase`.`film_id`
                        where `user`.`id` = $id;
                    QUERY;
                    $res = mysqli_query($conn, $query);
                    while ($arr = mysqli_fetch_assoc($res)) {
                        ?>
                        <tr>
                            <td><?php echo $arr["title"] ?></td>
                            <td><?php echo $arr["date"] ?></td>
                            <td>
                                <a href=<?php echo "'/download.php?id=" . $arr["film_id"] . "'" ?>>
                                    <i class="fa fa-download" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            <div class="container2">

                <hr>
            </div>
            <div class="div-body">
                <h2>Rentals:</h2>
                <table style="text-align: center">
                    <thead>
                    <tr>
                        <th>Film</th>
                        <th>Start</th>
                        <th>End</th>
                        <th>Stream</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $id = mysqli_real_escape_string($conn, $_SESSION["user_id"]);
                    $query = <<<QUERY
                        select  `film`.`id` as `film_id`,
                                `film`.`title` as `title`,
                                `rental`.`start_date` as `start`,
                                `rental`.`end_date` as `end`
                        from `user` inner join `rental`
                                        on `user`.`id` = `rental`.`user_id`
                                    inner join `film`
                                        on `film`.`id` = `rental`.`film_id`
                        where `user`.`id` = $id;
                    QUERY;
                    $res = mysqli_query($conn, $query);
                    while ($arr = mysqli_fetch_assoc($res)) {
                        ?>
                        <tr>
                            <td><?php echo $arr["title"] ?></td>
                            <td><?php echo $arr["start"] ?></td>
                            <td><?php echo $arr["end"] ?></td>
                            <td>
                                <?php
                                $now = date("Y-m-d");
                                if ($arr["end"] < $now) {
                                    ?>
                                    <i class="fa fa-exclamation-triangle"></i>
                                    <?php
                                }
                                else {
                                    ?>
                                    <a href=<?php echo "'/stream.php?id=" . $arr["film_id"] . "'" ?>>
                                        <i class="fa fa-play-circle" aria-hidden="true"></i>
                                    </a>
                                    <?php
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php
require_once("./lib/footer.php");
?>
<script src="./resource/static/js/admin.js"></script>
</body>
</html>

