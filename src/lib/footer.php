<footer class="bg-footer">
    <!-- Grid container -->
    <div class="container p-4">
        <!--Grid row-->
        <div class="row ">
            <!--Grid column-->
            <div class="col-sm-4  mb-4 mb-md-12 text-center ">
                <h5 class="text-uppercase gold">DEA KINEMA</h5>
                <a class="footer_a" href="/signup/signup.php">Register</a></br>
                <a class="footer_a" href="/login/login.php">LogIn</a></br>
                <a class="footer_a" href="#">Privacy Policy </a>
            </div>

            <div class="col-sm-4 mb-4 mb-md-12 text-center">
                <h5 class=" gold">Contact us</h5>
                <a class="footer_a" href="#">Help Center</a></br>
                <a class="footer_a" href="#">About Us</a>
            </div>
            <div class="col-sm-4 mb-4 mb-md-5 text-center">
                <h5 class="gold">Social Media</h5>

                <a class="btn btn-primary s-media"style="background-color: #3b5998;"href="#"role="button">
                    <i class="fa fa-facebook"></i>
                </a>
                <!-- Youtube -->
                <a class="btn btn-primary s-media" style="background-color: #ed302f;" href="#"  role="button">
                    <i class="fa fa-youtube"></i>
                </a>
                <a class="btn btn-primary s-media" style="background-color: #333333;" href="#" role="button">
                    <i class="fa fa-github"></i>
                </a>
                <!-- Stack overflow -->
                <a class="btn btn-primary s-media" style="background-color: #ffac44;" href="#" role="button">
                    <i class="fa fa-stack-overflow"></i>
                </a>
            </div>
        </div>
    </div>
</footer>

<div class="text-center copy-right p-3" style="background-color:black">
    © 2022 Copyright:
    <a class="copy-right" href="https://gitlab.com/fshn/programim-ne-web/kino.git">Grupi DEA</a>
</div>
