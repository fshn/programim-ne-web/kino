<?php
session_start();
require_once("./lib/connect.php");

if (!isset($_POST["film"]) || !isset($_SESSION["user_id"])) {
    print "Error!\n";
    print "User and film id are required!\n";
}
else {
    $film_id = mysqli_real_escape_string($conn, $_POST["film"]);
    $user_id = mysqli_real_escape_string($conn, $_SESSION["user_id"]);

    $res = mysqli_query(
        $conn,
        <<<QUERY
        select count(*)
        from `film`
        where `film`.`id` = $film_id;
        QUERY
    );
    $check = mysqli_fetch_array($res);
    if ($check[0] != 1) {
        print "Film with id = $film_id, does not exists.\n";
    }
    else {
        $now = date("Y-m-d");
        $end = date("Y-m-d", strtotime("+3 day"));
        $res = mysqli_query(
            $conn,
            <<<QUERY
            insert into `rental`(`film_id`, `user_id`, `start_date`, `end_date`)
            values ($film_id, $user_id, "$now", "$end");
            QUERY
        );
        if ($res) {
            ?>
            <h2>Success!</h2>
            <p><a href="/dashboard.php">Go to dashboard</a></p>
            <?php
        }
        else {
            print "Error while registing the purchase";
        }
    }
}

