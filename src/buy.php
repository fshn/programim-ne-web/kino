<?php
session_start();
require_once("./lib/connect.php");

if (!isset($_POST["film"]) || !isset($_SESSION["user_id"])) {
    print "Error!\n";
    print "User and film id are required!\n";
}
else {
    $film_id = mysqli_real_escape_string($conn, $_POST["film"]);
    $user_id = mysqli_real_escape_string($conn, $_SESSION["user_id"]);

    $res = mysqli_query(
        $conn,
        <<<QUERY
        select count(*)
        from `purchase`
        where `purchase`.`film_id` = $film_id and `purchase`.`user_id` = $user_id;
        QUERY
    );
    $check = mysqli_fetch_array($res);
    if ($check[0] != 0) {
        print "You already own this film.\n";
    }
    else {
        $res = mysqli_query(
            $conn,
            <<<QUERY
            select count(*)
            from `film`
            where `film`.`id` = $film_id;
            QUERY
        );
        $check = mysqli_fetch_array($res);
        if ($check[0] != 1) {
            print "Film with id = $film_id, does not exists.\n";
        }
        else {
            $now = date("Y-m-d");
            $res = mysqli_query(
                $conn,
                <<<QUERY
                insert into `purchase`(`purchase`.`film_id`, `purchase`.`user_id`, `purchase`.`purchase_date`)
                values ($film_id, $user_id, "$now");
                QUERY
            );
            if ($res) {
                ?>
                <h2>Success!</h2>
                <p><a href="/dashboard.php">Go to dashboard</a></p>
                <?php
            }
            else {
                print "Error while registing the purchase";
            }
        }
    }
}
?>
